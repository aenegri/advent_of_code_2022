def is_visible(row, col, rows, cols, tree_map, max_height_top, max_height_bottom, max_height_left, max_height_right) -> bool:
  tree_height = tree_map[(row, col)]
  height_top = max_height_top[(row-1, col)] if row >= 1 else -1
  height_bottom = max_height_bottom[(row+1, col)] if row <= rows - 2 else -1
  height_left = max_height_left[(row, col-1)] if col >= 1 else -1
  height_right = max_height_right[(row, col+1)] if col <= cols - 2 else -1
  return (
    tree_height > height_top or
    tree_height > height_bottom or
    tree_height > height_left or
    tree_height > height_right
  )
  
def scenic_score(row, col, rows, cols, tree_map) -> int:
  if row == 0 or row == rows - 1 or col == 0 or col == cols - 1:
    return 0
  score = 1
  height = tree_map[(row, col)]
  vrow, vcol, vheight = row, col, -1
  
  vrow, vcol, vheight = row - 1, col, tree_map[(row - 1, col)]
  while height > vheight and vrow >= 1:
    vrow -= 1
    vheight = tree_map[(vrow, vcol)]
  score *= row - vrow
  
  vrow, vcol, vheight = row + 1, col, tree_map[(row + 1, col)]
  while height > vheight and vrow <= rows - 2:
    vrow += 1
    vheight = tree_map[(vrow, vcol)]
  score *= vrow - row
  
  vrow, vcol, vheight = row, col - 1, tree_map[(row, col - 1)]
  while height > vheight and vcol >= 1:
    vcol -= 1
    vheight = tree_map[(vrow, vcol)]
  score *= col - vcol
  
  vrow, vcol, vheight = row, col + 1, tree_map[(row, col + 1)]
  while height > vheight and vcol <= cols - 2:
    vcol += 1
    vheight = tree_map[(vrow, vcol)]
  score *= vcol - col
  
  return score
  


if __name__ == "__main__":
  with open("input/day_8.txt") as f:
    tree_data = f.read()
    
  tree_map = dict()
  tree_data_lines = tree_data.split("\n")
  for row, line in enumerate(tree_data_lines):
    for col, tree in enumerate(line):
      tree_map[(row, col)] = int(tree)
    
  rows = len(tree_data_lines)  
  cols = len(tree_data_lines[0])
  
  max_height_top = dict()
  for col in range(0, cols):
    for row in range(0, rows):
      prev = max_height_top[(row-1, col)] if row >= 1 else 0
      curr = tree_map[(row, col)]
      max_height_top[(row, col)] = max(prev, curr)
      
  max_height_bottom = dict()
  for col in range(0, cols):
    for row in range(rows-1, -1, -1):
      prev = max_height_bottom[(row+1, col)] if row <= rows - 2 else 0
      curr = tree_map[(row, col)]
      max_height_bottom[(row, col)] = max(prev, curr)
      
  max_height_left = dict()
  for row in range(0, rows):
    for col in range(0, cols):
      prev = max_height_left[(row, col-1)] if col >= 1 else 0
      curr = tree_map[(row, col)]
      max_height_left[(row, col)] = max(prev, curr)
      
  max_height_right = dict()
  for row in range(0, rows):
    for col in range(cols-1, -1, -1):
      prev = max_height_right[(row, col+1)] if col <= cols - 2 else 0
      curr = tree_map[(row, col)]
      max_height_right[(row, col)] = max(prev, curr)

  visible_count = sum(int(is_visible(
    row, col, rows, cols, tree_map, max_height_top, max_height_bottom, max_height_left, max_height_right
    )) for row, col in tree_map.keys())
  print(f"{visible_count=}")
  
  max_scenic_score = max(scenic_score(row, col, rows, cols, tree_map) for row, col in tree_map.keys())
  print(f"{max_scenic_score=}")
  
  
    