from enum import IntEnum
import json
import functools

PacketObject = list['PacketObject']|int

class OrderResult(IntEnum):
  NOT_IN_ORDER = 1
  CONTINUE = 0
  IN_ORDER = -1

def compare_packets(left: PacketObject, right: PacketObject) -> OrderResult:
  # align types
  if isinstance(left, list) and isinstance(right, int):
    return compare_packets(left, [right])    
  elif isinstance(right, list) and isinstance(left, int):
    return compare_packets([left], right)
  elif isinstance(left, int) and isinstance(right, int):
    if left < right:
      return OrderResult.IN_ORDER
    elif right < left:
      return OrderResult.NOT_IN_ORDER
    return OrderResult.CONTINUE
  
  # ===left and right are both lists===
    
  for lvalue, rvalue in zip(left, right):
    order = compare_packets(lvalue, rvalue)

    if order != OrderResult.CONTINUE:
      return order
        
  if len(left) < len(right):
    return OrderResult.IN_ORDER
  elif len(right) < len(left):
    return OrderResult.NOT_IN_ORDER
  
  return OrderResult.CONTINUE
    
    
  

if __name__ == "__main__":
  with open("input/day_13.txt") as f:
    data = f.read()
    
  pairs = []  
  for chunk in data.split("\n\n"):
    lines = [line.strip() for line in chunk.split("\n")]
    left, right = json.loads(lines[0]), json.loads(lines[1])
    pairs.append((left, right))
      
  idx_sum = 0
  for idx, pair in enumerate(pairs):
    if compare_packets(pair[0], pair[1]) in [OrderResult.IN_ORDER, OrderResult.CONTINUE]:
      idx_sum += idx + 1
      
  print(f"Ordered index sum: {idx_sum}")
      
  all_packets = [[[2]], [[6]]]
  for pair in pairs:
    all_packets.extend(pair)
    
  sorted_packets = sorted(all_packets, key=functools.cmp_to_key(compare_packets))
  decoder_key = (sorted_packets.index([[2]]) + 1) * (sorted_packets.index([[6]]) + 1)
  
  print(f"Decoder key: {decoder_key}")
  