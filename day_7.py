
class FileSystemParsingException(Exception):
  pass

class FileSystemNode:
  def __init__(self, name: str, dir: bool, size: int):
    self.name = name
    self.dir = dir
    self.size = size
    self._children: dict[str, FileSystemNode] = dict()
    
  def __repr__(self) -> str:
    return f"FileSystemNode<name={self.name}, dir={self.dir}, size={self.size}>"
    
  def add_child(self, child: 'FileSystemNode') -> None:
    self._children[child.name] = child
    
  def get_child(self, name: str) -> 'FileSystemNode':
    return self._children[name]
  
  def children(self) -> dict[str, 'FileSystemNode']:
    return self._children
  

class FileSystemParser:
  def __init__(self):
    self._nav_stack = [FileSystemNode("/", True, 0)]
    
  def _cd(self, destination: str) -> None:
    if destination == "/":
      self._nav_stack = self._nav_stack[:1]
    elif destination == "..":
      self._nav_stack.pop()
    else:
      destination_node = self._nav_stack[-1].get_child(destination)
      self._nav_stack.append(destination_node)
    
  def _ls(self, output: list[str]):
    curr_node = self._nav_stack[-1]
    
    for line in output:
      match line.split():
        case ["dir", name]:
          curr_node.add_child(FileSystemNode(name, True, 0))
        case [size, name]:
          curr_node.add_child(FileSystemNode(name, False, int(size)))
        case []:
          pass  # empty line
        case _:
          raise FileSystemParsingException(f"Unexpected ls output: {line}")
  
  def parse(self, terminal_output: str) -> FileSystemNode:
    raw_actions = terminal_output.split("$ ")[1:]  # first entry is empty str
          
    for raw_action in raw_actions:
      input, *output = raw_action.split("\n")
      
      match input.split():
        case ["cd", destination]:
          self._cd(destination)
        case ["ls"]:
          self._ls(output)
        case _:
          raise FileSystemParsingException(f"Unexpected terminal command: {input}")
    
    return self._nav_stack[0]
  
def size_all_dirs(node: FileSystemNode, paths: list[str], dir_size: dict[str, int]) -> dict[str, int]:
  total_size = 0
  
  if node.name == "/":
    abs_path = "/"
  else:
    abs_path = "/".join(paths)[1:] + f"/{node.name}"
      
  for child in node.children().values():
    if child.dir:
      paths.append(node.name)
      size_all_dirs(child, paths, dir_size)
      paths.pop()
      child_path = f"/{child.name}" if abs_path == "/" else f"{abs_path}/{child.name}"
      total_size += dir_size[child_path]
    else:
      total_size += child.size
  
  dir_size[abs_path] = total_size
  
def at_most(dir_size: dict[str, int], bound: int) -> dict[str,int]:
  return {dir: size for dir, size in dir_size.items() if size <= bound}

def smallest_above(dir_size: dict[str, int], bound: int) -> tuple[str, int]:
  smallest_dir, smallest_size = "/", dir_size["/"]
  for dir, size in dir_size.items():
    if size > bound and size < smallest_size:
      smallest_dir, smallest_size = dir, size
  return smallest_dir, smallest_size

if __name__ == "__main__":
  with open("input/day_7.txt") as f:
    data = f.read()
  parser = FileSystemParser()
  root = parser.parse(data)
  dir_size = dict()
  size_all_dirs(root, [], dir_size)
  max_total_size = 100000
  total_size_under_threshold = sum(at_most(dir_size, 100000).values())
  
  disk_size = 70000000
  unused_space_target = 30000000
  unused_space = disk_size - dir_size["/"]
  min_to_delete = unused_space_target - unused_space
  print(f"Size of all dirs that are at most {100000}: {total_size_under_threshold}")
  print(f"Remove dir {smallest_above(dir_size, min_to_delete)}")
