from collections import deque
from typing import Callable

neighbors = [(1, 0), (-1, 0), (0, 1), (0, -1)]

def in_bounds(row: int, col: int, grid: list[list[any]]) -> bool:
  return (
    row >= 0 and row < len(grid) and
    col >= 0 and col < len(grid[0])
  )
  
def can_step_forward(curr_height: str, next_height: str) -> bool:
  return ord(next_height) - ord(curr_height) <= 1

def can_step_reverse(curr_height: str, next_height: str) -> bool:
  return ord(curr_height) - ord(next_height) <= 1

def bfs(start: tuple[int], end: tuple[int]|str, heightmap: list[list[int]], can_step: Callable[[str, str], bool]) -> int:
  curr_steps, next_steps = deque(), deque()
  visited = set()
  count = 0
  curr_steps.append(start)
  while curr_steps:
    while curr_steps:
      curr_row, curr_col = curr_steps.popleft()
      curr_height = heightmap[curr_row][curr_col]
            
      if (isinstance(end, str) and end == heightmap[curr_row][curr_col]) or end == (curr_row, curr_col):
        return count
      
      for drow, dcol in neighbors:
        neighbor_row, neighbor_col = curr_row + drow, curr_col + dcol
        
        if not in_bounds(neighbor_row, neighbor_col, heightmap):
          continue
        
        neighbor_height = heightmap[neighbor_row][neighbor_col]
        
        if (
          (neighbor_row, neighbor_col) not in visited and
          can_step(curr_height, neighbor_height)
        ):
          next_steps.append((neighbor_row, neighbor_col))
          visited.add((neighbor_row, neighbor_col))
    
    curr_steps = next_steps
    next_steps = deque()
    count += 1
  
  return -1  # unsolvable or a bug
    
    

if __name__ == "__main__":
  with open("input/day_12.txt") as f:
    data = f.read()
    
  lines = data.split("\n")
  heightmap = [[] for _ in range(len(lines))]
  for row, line in enumerate(lines):
    for col, c in enumerate(line):
      heightmap[row].append(c)
      if c == "S":
        start = (row, col)
        heightmap[row][col] = "a"
      elif c == "E":
        end = (row, col)
        heightmap[row][col] = "z"
        
  print(f"Distance from start to end: {bfs(start, end, heightmap, can_step_forward)}")
      
  # You could brute force by calculating the shortest path from each a->E,
  # but we can do it in one iteration by reversing the bfs to be from E->a
  shortest_route = bfs(end, "a", heightmap, can_step_reverse)
  print(f"Shortest route from a to z: {shortest_route}")
      
      