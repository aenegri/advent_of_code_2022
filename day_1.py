if __name__ == "__main__":
  data = open("input/day_1.txt").read()
  calories_per_elf = sorted([sum(int(snack) for snack in elf.split("\n")) for elf in data.split("\n\n")], reverse=True)
  print(f"The elf with the most calories has {calories_per_elf[0]} calories")
  print(f"The top three elves have {sum(calories_per_elf[:3])} calories combined")
