from parse import ints

class GravitySim:
  def __init__(self, floor: bool):
    self.floor = floor
    self.grid = dict()
    self.rest_count = 0
    self.lowest_rock = 0
    
  def _flow_end(self, xsand, ysand) -> bool:
    if self.floor:
      return (xsand, ysand) == (500, 0)
    return ysand > self.lowest_rock

  def _drop_sand(self) -> bool:
    """
    Return `True` if sand flow has ended
    """
    xsand, ysand = 500, 0
    flowing, first_pass = True, True
    while (not self._flow_end(xsand, ysand) and flowing) or first_pass:
      first_pass = False
      options = [(xsand, ysand + 1), (xsand - 1, ysand + 1), (xsand + 1, ysand + 1)]
      flowing = False
      for xoption, yoption in options:
        if self._peek_grid(xoption, yoption) == ".":
          flowing = True
          xsand, ysand = xoption, yoption
          break
          
    if self._flow_end(xsand, ysand):
      return True
          
    self._update_grid(xsand, ysand, "o")
    return False
  
  def _update_grid(self, x: int, y: int, unit: str) -> None:
    self.grid[(x, y)] = unit
    
    if unit == "#":
      self.lowest_rock = max(self.lowest_rock, y)
    
  def _peek_grid(self, x: int, y: int) -> str:
    if (x, y) in self.grid:
      return self.grid[(x, y)]
    
    if self.floor and y == self.lowest_rock + 2:
      return "#"
    
    return "."
  
  def flow(self) -> int:
    """
    Return the number of sand units which come to rest before the flow terminates
    """
    while True:
      flow_stopped = self._drop_sand()
      if flow_stopped:
        return self.rest_count
      self.rest_count += 1
      
  def trace_rock_structure(self, path: list[tuple[int]]) -> None:
    for idx in range(1, len(path)):
      x1, y1 = path[idx-1]
      x2, y2 = path[idx]
      
      if x1 == x2:
        step = 1 if y2 > y1 else -1
        for y in range(y1, y2, step):
          self._update_grid(x1, y, "#")
      elif y1 == y2:
        step = 1 if x2 > x1 else -1
        for x in range(x1, x2, step):
          self._update_grid(x, y1, "#")
          
      self._update_grid(x2, y2, "#")
      

if __name__ == "__main__":
  # parse rocks
  with open("input/day_14.txt") as f:
    data = f.read()
    
  sim_no_floor = GravitySim(False)
  sim_with_floor = GravitySim(True)
  for rock_data in data.split("\n"):
    nums = ints(rock_data)
    path = [(nums[i], nums[i+1]) for i in range(0, len(nums), 2)]
    sim_no_floor.trace_rock_structure(path)
    sim_with_floor.trace_rock_structure(path)
    
  
  print(f"Sand units without floor: {sim_no_floor.flow()}")
  print(f"Sand units with floor: {sim_with_floor.flow() + 1}")
