from collections import deque
from typing import Generator

class InvalidInstructionException(Exception):
  pass

class VideoCPU:
  def __init__(self, instructions: deque[str]):
    self.instructions = instructions
    self.cycle = 0
    self.pending_cycles = 0
    self.pending_add = 0
    self.X = 1
    self.image = [["."] * 40 for _ in range(6)]
    
  def _update_crt(self):
    common_pixels = set(range(self.X - 1, self.X + 2)) & {(self.cycle - 1) % 40}

    if not common_pixels:
      return
    
    pixel = common_pixels.pop()
    
    col = (pixel) % 40
    row = (self.cycle - 1) // 40
    self.image[row][col] = "#"
  
  def render(self):
    for row in self.image:
      out = ""
      for pixel in row:
        out += pixel
      print(out)
    
  def run(self) -> Generator[int, None, None]:
    while self.instructions or self.pending_cycles > 0:
      self.cycle += 1
      
      if self.pending_add != 0 and self.pending_cycles == 0:
        self.X += self.pending_add
        self.pending_add = 0
              
      self._update_crt()
      
      if (self.cycle - 20) % 40 == 0:
        yield self.cycle * self.X
      
      if self.pending_cycles > 0:
        self.pending_cycles -= 1
        continue
      
      instruction = self.instructions.popleft()
      
      match instruction.split():
        case ["addx", arg]:
          self.pending_cycles = 1
          self.pending_add = (int(arg))
        case ["noop"]:
          continue
        case _:
          raise InvalidInstructionException(f"Unable to run instruction: {instruction}")
    

if __name__ == "__main__":
  with open("input/day_10.txt") as f:
    data = f.read()
    
  sanitized_instructions = deque(line.strip() for line in data.split("\n"))
  
  cpu = VideoCPU(sanitized_instructions)
  signal_strength_sum = sum(cpu.run())
  print(f"Signal strength: {signal_strength_sum}\n")
  cpu.render()
  