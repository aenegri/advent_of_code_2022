from dataclasses import dataclass
from typing import Iterable, Optional

from perf import print_elapsed_time

@dataclass
class Node:
  v: int
  prev: Optional['Node']
  next: Optional['Node']
  
class LinkedList:
  def __init__(self, data: Iterable[int], key=1):
    self.head = Node(next(data)*key, None, None)
    self.initial_order = [self.head]
    
    prev, curr = self.head, None
    self._len = 1
    for v in data:
      curr = Node(v*key, prev, None)
      prev.next = curr
      self.initial_order.append(curr)
      
      if v == 0:
        self.zero = curr
      prev = curr
      self._len += 1
      
    # make the linked list circular
    curr.next = self.head
    self.head.prev = curr
    
  def __len__(self):
    return self._len
    
  def get_coordinates(self):
    idx = 0
    curr = self.zero
    while idx <= 3000:
      if idx % 1000 == 0:
        yield curr.v
      curr = curr.next
      idx += 1
  
  def mix(self):
    for node in self.initial_order:
      shift = abs(node.v) % (len(self) - 1)
      target = node
      if shift == 0:
        continue
      elif node.v > 0:
        while shift:
          target = target.next
          shift -= 1
      else:
        shift += 1
        while shift:
          target = target.prev
          shift -= 1
          
      # remove node from current links
      prev = node.prev
      prev.next = node.next
      node.next.prev = prev
      # insert node in front of target
      node.next = target.next
      node.prev = target
      target.next = node
      node.next.prev = node
      
if __name__ == "__main__":
  with open("input/day_20.txt") as f:
    data = f.read()
    
  nums = [int(line) for line in data.split("\n")]
  
  with print_elapsed_time("Part 1"):
    linked_list = LinkedList(iter(nums))
    linked_list.mix()
    print(f"Coordinate sum w/o decryption key: {sum(linked_list.get_coordinates())}")
  
  with print_elapsed_time("Part 2"):
    linked_list = LinkedList(iter(nums), key=811589153)
    for _ in range(10):
      linked_list.mix()
    print(f"Coordinate sum w/ decryption key: {sum(linked_list.get_coordinates())}")
