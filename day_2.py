
decode_move = {"X": "A", "Y": "B", "Z": "C"}

hand_points = {
  "A": 1,
  "B": 2,
  "C": 3,
}

move_to_idx = {"A": 0, "B": 1, "C": 2}
all_moves = list(move_to_idx.keys())

def wins_against(move: str):
  return all_moves[(move_to_idx[move] + 1) % 3]

def choose_move(enemy_move: str, result: str):
  if result == "X":
    return wins_against(wins_against(enemy_move))
  elif result == "Y":
    return enemy_move
  return wins_against(enemy_move)

def calc_score(enemy_move: str, player_move: str):
  if enemy_move == player_move:
    result_points = 3
  elif wins_against(enemy_move) == player_move:
    result_points = 6
  else:
    result_points = 0
  
  return result_points + hand_points[player_move]

def score_round_a(round: list[str]):
  enemy_move, player_move = round
  return calc_score(enemy_move, decode_move[player_move])

def score_round_b(round: list[str]):
  enemy_move, result = round
  player_move = choose_move(enemy_move, result)
  return calc_score(enemy_move, player_move)

if __name__ == "__main__":
  rounds = [round.split(" ") for round in open("input/day_2.txt").read().split("\n")]
  total1 = sum(score_round_a(round) for round in rounds)
  total2 = sum(score_round_b(round) for round in rounds)
  print(f"First strategy score is {total1}")
  print(f"Second strategy score is {total2}")
