from dataclasses import dataclass
from typing import Generator
    
@dataclass
class Coord:
  x: int
  y: int
  
  def vector_to(self, other: 'Coord') -> tuple[int]:
    return other.x - self.x, other.y - self.y
  
  def as_tuple(self) -> tuple[int]:
    return self.x, self.y

class Motion:
  DIR_TO_VECTOR = {
    "U": (0, 1),
    "D": (0, -1),
    "L": (-1, 0),
    "R": (1, 0)
  }
  
  def __init__(self, dir: str, times: int) -> None:
    self.dir = dir
    self.times = times
  
  def __iter__(self) -> Generator[tuple[int], None, None]:
    return (self.DIR_TO_VECTOR[self.dir] for _ in range(0, self.times))
  
  def __repr__(self) -> str:
    return f"Motion(dir={self.dir}, times={self.times})"
    
class KnotGrid:
  def __init__(self, num_knots: int) -> None:
    self.knots = [Coord(0,0) for _ in range(0, num_knots)]
    self.tail_visited: set[tuple[int]] = set()
    self.tail_visited.add((0,0))
    
  def run(self, motion: Motion) -> None:
    for dx, dy in motion:
      self.knots[0].x += dx
      self.knots[0].y += dy
      
      self._update_knots()
      self.tail_visited.add(self.knots[-1].as_tuple())
        
  def _update_knots(self):
    for idx in range(1, len(self.knots)):
      prev, curr = self.knots[idx-1], self.knots[idx]
      
      dx, dy = curr.vector_to(prev)
      if abs(dx) < 2 and abs(dy) < 2:
        return  # adjacent
      
      next_pos = Coord(curr.x, curr.y)
      
      if bool(dx):
        next_pos.x += 1 if dx > 0 else -1
      if bool(dy):
        next_pos.y += 1 if dy > 0 else -1
        
      self.knots[idx] = next_pos
        
if __name__ == "__main__":
  with open("input/day_9.txt") as f:
    data = f.read().strip()
    
  motions = []
  for line in data.split("\n"):
    l, r = line.split()
    motions.append(Motion(l, int(r)))
    
  grid1 = KnotGrid(2)
  for m in motions:
    grid1.run(m)
  print(len(grid1.tail_visited))
  
  grid2 = KnotGrid(10)
  for m in motions:
    grid2.run(m)
  print(len(grid2.tail_visited))
  