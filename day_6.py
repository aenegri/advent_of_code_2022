import collections

def first_distinct_sequence_end(datastream: str, distinct_count: int) -> int:
  window = collections.defaultdict(int)
  
  for idx in range(distinct_count-1):
    window[datastream[idx]] += 1
    
  for idx in range(distinct_count-1, len(datastream)):
    c = datastream[idx]
    window[c] += 1
      
    if len(window) == distinct_count:
      return idx+1
    
    window_start = datastream[idx-distinct_count+1]
    window[window_start] -= 1
    if window[window_start] == 0:
      del window[window_start] 

  return -1

if __name__ == "__main__":
  datastream = open("input/day_6.txt").read()
  print(f"First start of packet index: {first_distinct_sequence_end(datastream, 4)}")
  print(f"First start of message index: {first_distinct_sequence_end(datastream, 14)}")
  