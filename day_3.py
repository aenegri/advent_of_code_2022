def get_priority(item: str) -> int:
  if ord("z") >= ord(item) >= ord("a"):
    return ord(item) - ord("a") + 1
  return ord(item) - ord("A") + 27

def rucksack_item(sack: str) -> str:
  midpoint = len(sack) // 2
  left, right = sack[:midpoint], sack[midpoint:]
  return (set(left) & set(right)).pop()

def badge_item(x: str, y: str, z: str) -> str:
  return (set(x) & set(y) & set(z)).pop()

if __name__ == "__main__":
  rucksacks = open("input/day_3.txt").read().split("\n")
  
  rucksack_priority = sum(get_priority(rucksack_item(sack)) for sack in rucksacks)
  print(f"Rucksack priority total: {rucksack_priority}")
  
  badge_priority = sum(get_priority(badge_item(rucksacks[i],rucksacks[i+1], rucksacks[i+2]))for i in range(0, len(rucksacks), 3))
  print(f"Badge priority total: {badge_priority}")
    
    
    
    