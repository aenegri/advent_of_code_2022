from collections import deque
from typing import NamedTuple, Optional
from parse import ints
import re

class Op(NamedTuple):
  op: str
  arg: str

class Monkey:
  def __init__(self, id: int, items: deque[int], op: Op, test: int, test_pass: int, test_fail: int):
    self.id = id
    self.items = items
    self.op = op
    self.test = test
    self.test_pass = test_pass
    self.test_fail = test_fail
    self.inspect_count = 0
    
  def append(self, item: str):
    self.items.append(item)
    
  def process_items(self, worry_divisor: int, worry_mod: Optional[int] = None):
    while self.items:
      item = self.items.popleft()
      self.inspect_count += 1
      item = self._apply_op(item)
      item = item // worry_divisor
      if worry_mod:
        item %= worry_mod
      if item % self.test == 0:
        yield self.test_pass, item
      else:
        yield self.test_fail, item
      
  def _apply_op(self, item: int) -> int:
      operand = item if self.op.arg == "old" else int(self.op.arg)
      if self.op.op == "*":
        item *= operand
      elif self.op.op == "+":
        item += operand
      return item
      
class Clan:
  def __init__(self):
    self.monkeys: list[Monkey] = []
    self.mod_by = 1
  
  def append(self, monkey: Monkey):
    self.monkeys.append(monkey)
    self.mod_by *= monkey.test
    
  def monkey_business(self, rounds: int, mode: str) -> int:
    for _ in range(rounds):
      for monkey in self.monkeys:
        worry_divisor = 1 if mode == "2" else 3
        worry_mod = self.mod_by if mode == "2" else None
        for id, item in monkey.process_items(worry_divisor, worry_mod):
          self.monkeys[id].append(item)
    
    inspections = sorted((monkey.inspect_count for monkey in self.monkeys), reverse=True)
    return inspections[0] * inspections[1]

if __name__ == "__main__":
  with open("input/day_11.txt") as f:
    data = f.read()
  
  clan1 = Clan()
  clan2 = Clan()
  for monkey_data in data.split("\n\n"):
    lines = [line.strip() for line in monkey_data.split("\n")]
    id = ints(lines[0])[0]
    items = deque(ints(lines[1]))
    divisible_by = ints(lines[3])[0]
    test_pass = ints(lines[4])[0]
    test_fail = ints(lines[5])[0]
    op_match = re.search(r"old\s([*+])\s(old|\d+)", lines[2])
    clan1.append(Monkey(id, items, Op(op_match.group(1), op_match.group(2)), divisible_by, test_pass, test_fail))
    clan2.append(Monkey(id, items.copy(), Op(op_match.group(1), op_match.group(2)), divisible_by, test_pass, test_fail))

    
  print(f"🐒1: {clan1.monkey_business(20, '1')}")
  print(f"🐒2: {clan2.monkey_business(10000, '2')}")
  