
from contextlib import contextmanager
import time


@contextmanager
def print_elapsed_time(label: str):
  start = time.perf_counter()
  yield
  end = time.perf_counter()
  elapsed = end - start
  print(f"{label}: {elapsed}s")