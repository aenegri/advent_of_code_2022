def full_overlap(x: tuple[int], y: tuple[int]) -> bool:
  return (y[0] >= x[0] and y[1] <= x[1]) or (x[0] >= y[0] and x[1] <= y[1])

def any_overlap(x: tuple[int], y: tuple[int]) -> bool:
  return y[0] <= x[1] and x[0] <= y[1]

if __name__ == "__main__":
  data = open("input/day_4.txt").read().split("\n")
  clean_assignments = []
  for line in data:
    left, right = line.split(",")
    lmin, lmax = (int(x) for x in left.split("-"))
    rmin, rmax = (int(x) for x in right.split("-"))
    clean_assignments.append(((lmin, lmax), (rmin, rmax)))
    
  fully_overlap_count = sum(int(full_overlap(pair[0], pair[1])) for pair in clean_assignments)
  print(f"Assignments that fully overlap: {fully_overlap_count}")
  
  any_overlap_count = sum(int(any_overlap(pair[0], pair[1])) for pair in clean_assignments)
  print(f"Assignments that overlap at all: {any_overlap_count}")
