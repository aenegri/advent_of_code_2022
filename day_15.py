import collections
from parse import ints

def _reduce_ranges(ranges: list[tuple[int]]) -> list[tuple[int]]:
  ranges.sort(key=lambda range: range[0])
  
  reduced_ranges = ranges[:1]
  ranges = ranges[1:]
  for range in ranges:
    prev_start, prev_end = reduced_ranges[-1]
    curr_start, curr_end = range
    
    if curr_start <= prev_end:
      reduced_ranges[-1] = (prev_start, max(curr_end, prev_end))
    else:
      reduced_ranges.append(range)
      
  return reduced_ranges

def process_tunnel_network(coords: list[list[int]]) -> dict[int, list]:
  row_ranges: dict[int, list] = collections.defaultdict(list)
  
  for xsensor, ysensor, xbeacon, ybeacon in coords:
    dist = abs(xbeacon - xsensor) + abs(ybeacon - ysensor)
    
    for y in range(ysensor - dist, ysensor + dist + 1):
      xdist = dist - abs(ysensor - y)
      if xdist >= 1:
        row_ranges[y].append((xsensor - xdist, xsensor + xdist))
        
  for y, ranges in row_ranges.items():
    row_ranges[y] = _reduce_ranges(ranges)
    
  return row_ranges  

def range_coverage(row_ranges: dict[int, list[tuple[int]]], y: int) -> int:
  return sum((range[1] - range[0] for range in row_ranges[y]))

def tuning_frequency(row_ranges: dict[int, list[tuple[int]]], bound: int) -> int:
  for y in range(bound + 1):
    ranges = row_ranges[y]
    
    if len(ranges) < 2:
      continue
    
    for i in range(1, len(ranges)):
      prev_end = ranges[i - 1][1]
      curr_start = ranges[i][0]
      
      if prev_end + 1 > bound:
        break
    
      if curr_start - prev_end == 2:
        return (prev_end + 1) * bound + y
    
if __name__ == "__main__":
  with open("input/day_15.txt") as f:
    data = f.read()
    
  row_ranges = process_tunnel_network([ints(line) for line in data.split("\n")])
  coverage = range_coverage(row_ranges, 2000000)
  print(f"Positions that cannot contain a beacon: {coverage}")
  bound = 4000000
  print(f"Tuning frequency: {tuning_frequency(row_ranges, bound)}")
  
  