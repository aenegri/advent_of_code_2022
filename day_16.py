import copy
from dataclasses import dataclass
from collections import defaultdict, deque
import itertools
import re

from perf import print_elapsed_time

@dataclass
class Valve:
  flow: int
  open: bool
  connections: list[str]
  
ValveEdges = dict[str, dict[str, int]]

class ValvePath:
  def __init__(self, minutes: int):
    self.minutes = minutes
    self.paths = ["AA"]
    self.pressure = 0
    
  def append(self, id: str, dist: int, flow: int):
    self.paths.append(id)
    self.minutes -= dist + 1
    self.pressure += flow * self.minutes
    
  def __eq__(self, other) -> bool:
    return self.pressure == other.pressure
  
  def __gt__(self, other) -> bool:
    return self.pressure > other.pressure

def path_permutations(valves: dict[str, Valve], edges: ValveEdges, minutes: int):
  stack: list[ValvePath] = [ValvePath(minutes)]
  completed_paths: list[ValvePath] = []
  while stack:
    path = stack.pop()
    id = path.paths[-1]
    
    options = [(id, valve) for id, valve in valves.items() if id not in path.paths and valve.flow > 0]
    branches: list[ValvePath] = []
    
    for other_id, other_valve in options:
      dist = edges[id][other_id]
      
      if path.minutes - dist - 1 <= 0:
        continue
      
      extended_path = copy.deepcopy(path)
      extended_path.append(other_id, dist, other_valve.flow)
      branches.append(extended_path)
    
    if branches:
      stack.extend(branches)
    else:
      completed_paths.append(path)
      
  return completed_paths

def most_pressure_two_paths(sorted_paths: list[ValvePath], min_pressure: int) -> int:
  most = 0
  for i, path_a in enumerate(sorted_paths):
    visited_a = set(path_a.paths[1:])  # exclude "AA"
    for path_b in sorted_paths[i+1:]:
      if path_a.pressure + path_b.pressure <= min_pressure:
        break
      visited_b = set(path_b.paths[1:])
      
      if len(visited_a & visited_b) == 0:
        most = max(path_a.pressure + path_b.pressure, most)
  return most
      
      

def get_edges(valves: dict[str, Valve]) -> ValveEdges:
  edges: ValveEdges = defaultdict(dict)
  for id, valve in valves.items():
    # BFS to get dist from current valve to all other valves
    queue, next_queue = deque(valve.connections), deque()
    visited = set(valve.connections)
    visited.add(id)
    dist = 1
    while queue:
      while queue:
        other_id = queue.popleft()
        
        # we only care about edges that can release pressure
        if valves[other_id].flow > 0:
          edges[id][other_id] = dist
        
        for other_conn in valves[other_id].connections:
          if other_conn not in visited:
            next_queue.append(other_conn)
            visited.add(other_conn)
      dist += 1
      queue = next_queue
      next_queue = deque()
  return edges
  
if __name__ == "__main__":
  with open("input/day_16.txt") as f:
    data = f.read()
  
  valves: dict[str, Valve] = dict()
  for line in data.split("\n"):
    match = re.search(r"Valve (\w+) has flow rate=(\d+); tunnels? leads? to valves? ([\w,\s]+)", line)
    connections = [v.strip() for v in match[3].split(",")]
    valves[match[1]] = Valve(int(match[2]), False, connections)
  
  with print_elapsed_time("Building edges"):
    edges = get_edges(valves)  
  
  with print_elapsed_time("Path permutations with 30 minutes"):
    permutations = path_permutations(valves, edges, 30)
    print(len(path_permutations(valves, edges, 30)))
    
  with print_elapsed_time("Sorting"):
    permutations.sort(reverse=True)
    most_no_help = permutations[0].pressure
  
  print(f"Best path: {most_no_help}")
  
  with print_elapsed_time("Path permutations with 26 minutes"):
    permutations = path_permutations(valves, edges, 26)

  with print_elapsed_time("Sorting"):
    permutations.sort(reverse=True)
    
  with print_elapsed_time("Finding optimal two paths"):
    most = most_pressure_two_paths(permutations, most_no_help)
    print(f"Best path with assistant: {most}")
