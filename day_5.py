from collections import namedtuple
import copy
from parse import ints

CraneMove = namedtuple("CraneMove", ("times", "start", "end"))

def concat_top_of_piles(piles: list[list[str]]):
  tops = ""
  for pile in piles:
    if pile:
     tops += pile[-1]
  return tops.lstrip()

if __name__ == "__main__":
  data = open("input/day_5.txt").read()
  crate_input, move_input = data.split("\n\n")
  crate_lines = crate_input.split("\n")
  last_pile = ints(crate_lines[-1])[-1]
  piles_a = [[] for x in range(last_pile+1)]
  
  for row in reversed(crate_lines):
    for idx in range(1, len(row), 4):
      if row[idx].isalpha():
        piles_a[(idx+3) // 4].append(row[idx])
  
  piles_b = copy.deepcopy(piles_a)
  
  moves = [CraneMove(*ints(row)) for row in move_input.split("\n")]
  
  for move in moves:
    for _ in range(move.times):
      piles_a[move.end].append(piles_a[move.start].pop())
      
  print(concat_top_of_piles(piles_a))
  
  for move in moves:
    chunk = piles_b[move.start][-move.times:]
    piles_b[move.start] = piles_b[move.start][:-move.times]
    piles_b[move.end].extend(chunk)
    
  print(concat_top_of_piles(piles_b))